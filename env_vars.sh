#!/bin/bash
releases_file_path="/opt/releases.txt"

if test -f "$releases_file_path"; then
   export ODOO_URL=$(awk -F ': ' '/ODOO_URL/ {print $2}' $releases_file_path)
   export PGADMIN_URL=$(awk -F ': ' '/PGADMIN_URL/ {print $2}' $releases_file_path)
else
   echo "Les URL par défaut des variables ODOO_URL and PGADMIN_URL, vont être utilisées"
fi

exec "$@"